module.exports = {
  lintOnSave: false,
  pluginOptions: {
    quasar: {
      importStrategy: "kebab",
      rtlSupport: false,
    },
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/styles/quasar.scss";`,
      },
    },
  },
  transpileDependencies: ["quasar"],
};
