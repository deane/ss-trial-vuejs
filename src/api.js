import axios from "axios";

// let baseUrl = "https://flexi-comms-api.simplesoft.nz/";
let baseUrl = "http://localhost:9080";

let api = axios.create({
  baseURL: baseUrl,
  withCredentials: true,
});

export default api;
