import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: () =>
        import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue"),
    },
    {
      path: "/page2",
      name: "page2",
      component: () =>
        import(/* webpackChunkName: "page2" */ "../views/Page2.vue"),
    },
  ],
});
