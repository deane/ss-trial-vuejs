import api from "@/api.js";

export default {
  state: {
    message: "",
  },
  mutations: {
    setMessage(state, message) {
      state.message = message;
    },
  },
  actions: {
    // A Simple Get Request, To run this, run $store.dispatch('loadMessage');

    async loadMessage({ commit }) {
      let test = await api.get("test");
      commit("setMessage", test.data);
    },

    // A Simple POST Request, To run this, run $store.dispatch('newUser');

    // async newUser({ dispatch }, payload) {
    //   let returnData = {};
    //   await api
    //     .post("users", payload)
    //     .then((response) => {
    //       if (response.data.Result == "Success") {
    //         returnData = {
    //           Result: "Success",
    //           Response: response.data,
    //         };
    //       } else {
    //         returnData = {
    //           Result: "Failed",
    //           Response: response.data,
    //         };
    //       }
    //     })
    //     .catch((err) => {
    //       returnData = {
    //         Result: "Error",
    //         Response: err,
    //       };
    //     });
    //   dispatch("loadUsers");
    //   return returnData;
    // },

    // A Simple PUT Request, To run this, run $store.dispatch('saveUser');

    // async saveUser({ dispatch }, payload) {
    //   let returnData = {};
    //   await api
    //     .put(`users`, payload)
    //     .then((response) => {
    //       if (response.data.Result == "Success") {
    //         returnData = {
    //           Result: "Success",
    //           Response: response.data,
    //         };
    //       } else {
    //         returnData = {
    //           Result: "Failed",
    //           Response: response.data,
    //         };
    //       }
    //     })
    //     .catch((err) => {
    //       returnData = {
    //         Result: "Error",
    //         Response: {
    //           Message: "There was an Error, Please try again later",
    //         },
    //       };
    //     });
    //   dispatch("loadUsers");
    //   return returnData;
    // },
  },
  getters: {
    message(state) {
      return state.message;
    },
  },
};
