## Prerequisites for running frontend and backend for project
- Backend repository: https://gitlab.com/simplesoft/ss-trial-php
- NodeJS LTS: https://nodejs.org/en/
- Vue CLI: `npm install -g @vue/cli`
- Docker Desktop: https://www.docker.com/products/docker-desktop
- The frontend has been designed using Quasar UI framework (v1 to be compatible with Vue2): https://v1.quasar.dev/

## To start backend server
- From inside project folder run `docker-compose up` or `docker-compose up -d` if you wish to run it in the background (`docker-compose down` to stop background process)

## Database connection information
The docker-compose setup will start a MariaDB database for the project. We highly recommend installing HeidiSQL to work directly on the database. You can find this at https://www.heidisql.com/.
The username and password for connecting to the database can be found in the `docker-compose.yml` file.
The server will be available to heidi on `localhost:5306` 

## To setup frontend
- Run `npm install` inside project folder

## To run frontend
- Run `npm run serve` to start frontend


## Project Brief
This project is designed to expose you to all the technologies used at Simple Software and allow you to demonstrate your learning to date.

**Project Requirements**
- Create a simple todo application
- The application should show a list of your todo items
- Allow you to add new items
- Allow editing of existing items
- An easy way to mark items as complete

Feel free to add as much or as little to this project as you wish.
